require stream,2.8.10
require sr570,develop
 
#Specify the TCP endpoint and give your 'bus' an arbitrary name eg. "asynstream1".
drvAsynSerialPortConfigure ("asynport2","/dev/ttyS0")
asynSetOption ("asynport2", 0, "baud", "9600")
asynSetOption ("asynport2", 0, "bits", "8")
asynSetOption ("asynport2", 0, "parity", "none")
asynSetOption ("asynport2", 0, "stop", "2")
asynSetOption ("asynport2", 0, "clocal", "N")
asynSetOption ("asynport2", 0, "crtscts", "N")

#Indicate the folder where the SR570 protocol file is installed
epicsEnvSet("STREAM_PROTOCOL_PATH","$(sr570_DB)")

#Load your database defining the EPICS records
dbLoadRecords("stf.db")

iocInit()

